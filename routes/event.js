var express = require("express")
var router = express.Router()
var path = require("path")

var EventDB = {}

function getRandomNumber() {
    return Math.floor(Math.random() * (899999)) + 100000
}

function getUniqueEventID() {
    var nextEvent = getRandomNumber()

    while(nextEvent.toString() in EventDB) {
        nextEvent = getRandomNumber()
        console.log("finding number!")
    }

    return nextEvent
}

function clearEventID(eventID) {
    delete EventDB[eventID.toString()]
}

function hasValidSessionKey(eventID, modKey) {
    if (EventDB[eventID].moderatorKey === modKey) {
        return true
    }
    return false
}

function validModeratorKey(req, res, next) {
    if (req.session.user && req.session.user.eventID && req.session.user.moderatorKey) {
        if (hasValidSessionKey(req.session.user.eventID, req.session.user.moderatorKey)) {
            next()
        } else {
            res.redirect("/")
        }
    } else {
       res.redirect("/")
    }
}


router.get("/join/:eventID", function(req, res, next){

    if (req.params.eventID.toString() in EventDB) {
        // res.send("you joined room " + req.params.eventID)
        if (!req.session.user) {
            req.session.user = {}
        }

        req.session.user.questionForId = req.params.eventID.toString()
        res.sendFile(path.join(__dirname, "../public", "askquestion.html"))
    } else {
        res.redirect("/")
    }
})

router.get("/", function(req, res, next){
    res.send("Not a valid room")
})

router.post("/ask", function(req, res, next){
    req.sanitize("questionText").escape()
    req.sanitize("questionText").trim()

    var tEventId

    if (req.session.user) {
        tEventId = req.session.user.questionForId
    }

    var errors = req.validationErrors()

    if (errors) {
        res.send("Invalid input.")
    } else if (tEventId) {
        if (EventDB[tEventId].question.questions) {
            EventDB[tEventId].question.questions.push(req.body.questionText)
        }
        // res.send("Thanks for submitting!")
        res.sendFile(path.join(__dirname, "../public/thanks.html"))
    } else {
        res.redirect("/")
    }
})

router.get("/create", function(req, res, next){

    // If already hosting an event, erase the old one
    if (req.session.user && hasValidSessionKey(req.session.user.eventID, req.session.user.moderatorKey)){
        clearEventID(req.session.user.eventID)
    }

    req.session.user = {eventID: getUniqueEventID(), moderatorKey: getRandomNumber()}

    EventDB[req.session.user.eventID.toString()] = {
        moderatorKey: req.session.user.moderatorKey,
        question: {
            questionIndex: -1,
            questions: []
        }
    }

    // res.send("You created event. Here is your moderator key: ")
    // res.sendFile(path.join(__dirname, "../public", "create.html"))
    res.redirect("/event/manage")
})

router.get("/session", validModeratorKey, function (req, res, next) {
    res.jsonp({eventID: req.session.user.eventID,
                moderatorKey: req.session.user.moderatorKey,
                question: EventDB[req.session.user.eventID].question
    })
})


router.get("/manage", validModeratorKey, function(req, res, next){
    // res.send("You are managing event, based on current session.")
    res.sendFile(path.join(__dirname, "../public", "manage.html"))
})

module.exports = router