function Question() {

    var that = this
    
    this.questionIndex = -1
    this.questions = []
    this.questionSpan = document.getElementById("currentQuestion")
    this.progressBar = document.getElementById("progressBar")
    this.pendingRequest = false

    this.keypressed = function(key) {

    }

    this.previousQuestion = function () {
        if (that.questionIndex <= 0) {
            return
        }

        that.questionIndex--

        that.updateQuestion()
    }

    this.nextQuestion = function () {
        if (that.questionIndex >= that.questions.length) {
            return
        }

        that.questionIndex++

        that.updateQuestion()
    }

    this.removeQuestion = function() {
        if (that.questions.length <= 0 ||
            that.questionIndex < 0 || that.questionIndex >= that.questions.length) {
            return
        }

        that.questions.splice(that.questionIndex, 1)
    }

    this.updateProgressBar = function() {
        if (that.questions.length <= 0 || that.questionIndex >= that.questions.length-1) {
            that.progressBar.setAttribute("style", "width: 100%;")
        } else {
            var tProgress = Math.floor( ((that.questionIndex+1) / that.questions.length) * 100)
            var newStyle = "width: " + tProgress.toString() + "%;"
            that.progressBar.setAttribute("style", newStyle)
        }
    }

    this.updateQuestionText = function () {
        if (that.questions.length > 0 && that.questionIndex === -1) {
            that.questionIndex = 0
        }
        if (that.questions.length <= 0 ||
            that.questionIndex < 0 || that.questionIndex >= that.questions.length) {
            that.questionSpan.innerHTML = "<span class='text-muted'>No Questions...</span>"
            return
        }

        that.questionSpan.innerHTML = that.questions[that.questionIndex]
    }

    this.updateQuestion = function() {
        if (!that.pendingRequest) {
            that.pendingRequest = true

            var request = new XMLHttpRequest()
            request.open("GET", "/event/session", true)

            request.onload = function() {
                if (this.status >= 200 && this.status < 400) {
                    // success
                    var data = JSON.parse(this.response)
                    that.questions = data.question.questions
                    that.updateQuestionText()
                }

                that.pendingRequest = false
            }

            request.send()
        }
        that.updateQuestionText()
        that.updateProgressBar()
    }
}

var myQuestion = new Question()

function copyLink() {
    var copyText = document.getElementById("linkInput")
    copyText.select()
    document.execCommand("Copy")
}

function ready() {
    var keySpan = document.getElementById("currentEventID")
    var questionSpan = document.getElementById("currentQuestion")
    var copyButtonSpan = document.getElementById("currentCopyButton")

    // get the eventID
    var request = new XMLHttpRequest()
    request.open("GET", "/event/session", true)

    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // success
            var data = JSON.parse(this.response)
            var copyButton = "<div class='input-group input-group-sm'><span class='input-group-btn'><button class='btn btn-outline-success' type='button' id='copyButton'>Copy Link</button></span><input id='linkInput' type='text' class='form-control' value='https://nrqna.herokuapp.com/event/join/" + data.eventID.toString() + "' readonly></div>"

            keySpan.innerHTML = "Event ID: " + data.eventID
            copyButtonSpan.innerHTML = copyButton

            if (data.question.questions.length > 0) {
            } else {
                questionSpan.innerHTML = "<span class='text-muted'>No Questions...</span>"
            }

            document.getElementById("copyButton").addEventListener("click", copyLink)
        }
    }

    request.send()

    document.getElementById("nextButton").addEventListener("click", myQuestion.nextQuestion)

    document.getElementById("previousButton").addEventListener("click", myQuestion.previousQuestion)

    myQuestion.updateQuestion()

    setInterval(myQuestion.updateQuestion, 5000)

    window.addEventListener("keydown", function(e) {
        if (e.defaultPrevented) {
            return
        }

        if (e.key === "n") {
           myQuestion.nextQuestion()
        } else if (e.key === "p") {
            myQuestion.previousQuestion()
        }
    })
}

if (document.readyState !== "loading") {
    ready()
} else {
    document.addEventListener("DOMContentLoaded", ready)
}
