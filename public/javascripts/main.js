
function buttonCheck() {
    var eventID = document.getElementById("eventID")
    var joinButton = document.getElementById("joinEvent")

    var isDisabled = joinButton.className.indexOf("disabled") !== -1

    if (eventID.value.length === 6) {
        joinButton.classList.remove("disabled")
        joinButton.classList.remove("btn-outline-secondary")

        joinButton.classList.add("btn-primary")

        joinButton.setAttribute("href", "/event/join/" + eventID.value.toString())
    } else if(!isDisabled) {
        joinButton.classList.add("disabled")
        joinButton.classList.add("btn-outline-secondary")
        joinButton.classList.remove("btn-primary")
        joinButton.setAttribute("href", "#")
    }
}

function ready() {
    var eventID = document.getElementById("eventID")

    eventID.addEventListener("keyup", buttonCheck)

    buttonCheck()
}

if (document.readyState !== "loading") {
    ready()
} else {
    document.addEventListener("DOMContentLoaded", ready)
}