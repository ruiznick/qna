function ready() {
    var eventSpan = document.getElementById("eventSpan")
    var keySpan = document.getElementById("keySpan")
    // get the eventID and moderator key
    var request = new XMLHttpRequest()
    request.open("GET", "/event/session", true)

    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // success
            var data = JSON.parse(this.response)

            eventSpan.innerHTML = data.eventID
            // keySpan.innerHTML = data.moderatorKey
        }
    }

    request.send()
}

if (document.readyState !== "loading") {
    ready()
} else {
    document.addEventListener("DOMContentLoaded", ready)
}
